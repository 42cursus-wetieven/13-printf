# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/02/26 09:33:16 by wetieven          #+#    #+#              #
#    Updated: 2021/04/21 17:03:05 by wetieven         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

# =============== #
# === TARGETS === #
# =============== #

NAME		=	libftprintf.a

ODIR		=	obj/

# =============== #
# === SOURCES === #
# =============== #

LIBFT		=	$(LDIR)libft.a
LDIR		=	libft/

HDIR		=	include/ $(LDIR) $(CVRT)

SDIR		=	src/
CVRT		=	$(SDIR)converters/

vpath %.h $(HDIR)
vpath %.c $(SDIR) $(CVRT)

SRCS		=	ft_printf.c \
				ptf_parsing.c \
				ptf_typecasting.c \
				ptf_text.c \
				ptf_integers.c \
				ptf_hexa.c \
				ptf_padding.c \
	   			vectors.c

# ====================== #
# === COMPILER SETUP === #
# ====================== #

CC			=	gcc
CFLAGS		=	-Wall -Wextra -Werror -O3 -fno-builtin
CINCS		=	$(addprefix -I, $(HDIR))

# ============= #
# === RULES === #
# ============= #

all			:	make_libft $(NAME)

# ~~~ Placeholders ~~~ #

HEADERS		=	$(SRCS:%.c=%.h) ptf_converters.h
OBJS		=	$(SRCS:%.c=$(ODIR)%.o)

# ~~~ Objects compilation  ~~~ #

$(ODIR)%.o	:	%.c $(HEADERS)
				mkdir -p $(ODIR)
				$(CC) $(CFLAGS) $(CINCS) -c $< -o $@

# ~~~ Libraries archiving  ~~~ #

$(NAME)		:	$(LIBFT) $(OBJS)
				cp $(LIBFT) $(NAME)
				ar rcs $(NAME) $(OBJS)

$(LIBFT)	:	make_libft

# ~~~ Actions ~~~ #

RM			=	rm -rf

make_libft	:
				$(MAKE) -C $(LDIR)

norm		:
				norminette include src libft

bonus		:	all

clean		:
				$(MAKE) clean -C $(LDIR)
				$(RM) $(ODIR)

fclean		:	clean
				$(MAKE) fclean -C $(LDIR)
				$(RM) $(NAME)

re			:	fclean all

.PHONY		:	all make_libft norm bonus clean fclean re
