/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_back.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/27 15:12:37 by wetieven          #+#    #+#             */
/*   Updated: 2021/01/04 09:03:59 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_back(t_list **lst_start, t_list *new_elem)
{
	if (!*lst_start)
		*lst_start = new_elem;
	else
	{
		ft_lstlast(*lst_start)->next = new_elem;
	}
}
