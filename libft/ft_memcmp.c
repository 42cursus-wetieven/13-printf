/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 15:37:21 by wetieven          #+#    #+#             */
/*   Updated: 2021/04/05 19:07:14 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_memcmp(const void *src, const void *cmp, size_t n)
{
	while (n--)
		if (*((unsigned char *)src++) != *((unsigned char *)cmp++))
			return (*((unsigned char *)--src) - *((unsigned char *)--cmp));
	return (0);
}
