/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/27 18:36:13 by wetieven          #+#    #+#             */
/*   Updated: 2021/04/05 18:53:24 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, void *(*fct)(void *), void (*del)(void *))
{
	t_list	*ret;
	t_list	*ptr;
	t_list	*new_elem;

	if (!lst)
		return (NULL);
	ret = ft_lstnew((*fct)(lst->content));
	if (!ret)
		return (NULL);
	lst = lst->next;
	ptr = ret;
	while (lst)
	{
		new_elem = ft_lstnew((*fct)(lst->content));
		if (!new_elem)
		{
			ft_lstclear(&ret, del);
			return (NULL);
		}
		ptr->next = new_elem;
		lst = lst->next;
		ptr = ptr->next;
	}
	return (ret);
}
