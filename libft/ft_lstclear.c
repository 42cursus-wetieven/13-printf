/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/27 17:35:43 by wetieven          #+#    #+#             */
/*   Updated: 2020/12/27 17:35:50 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstclear(t_list **lst, void (*del)(void *))
{
	t_list	*temp;
	t_list	*ptr;

	ptr = *lst;
	temp = ptr;
	while (temp)
	{
		ptr = temp;
		temp = ptr->next;
		ft_lstdelone(ptr, del);
	}
	*lst = NULL;
}
