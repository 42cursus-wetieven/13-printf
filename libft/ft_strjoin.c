/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/05 08:16:00 by wetieven          #+#    #+#             */
/*   Updated: 2021/01/06 12:26:03 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *pre, char const *post)
{
	char	*ret;
	int		ret_len;

	if (!pre || !post)
		return (NULL);
	ret_len = ft_strlen(pre) + ft_strlen(post) + 1;
	ret = malloc(sizeof(char) * (ret_len));
	if (!ret)
		return (NULL);
	ft_strcpy(ret, pre);
	ft_strlcat(ret, post, ret_len);
	return (ret);
}
