/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vectors.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/03 16:39:37 by wetieven          #+#    #+#             */
/*   Updated: 2021/04/16 15:55:39 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "vectors.h"

t_vctr	*vctr_init(size_t unit_size, size_t init_count)
{
	t_vctr	*vct;

	vct = malloc(sizeof(t_vctr));
	if (!vct)
		return (NULL);
	vct->data = ft_calloc(unit_size, init_count);
	if (!vct->data)
		return (NULL);
	vct->entries = 0;
	vct->capacity = init_count;
	vct->unit_size = unit_size;
	vct->init_count = init_count;
	vct->size_factor = 1;
	return (vct);
}

int	vctr_extend(t_vctr *vct)
{
	void	*old_data;

	old_data = vct->data;
	vct->size_factor++;
	vct->capacity = vct->init_count * vct->size_factor;
	vct->data = ft_calloc(vct->unit_size, vct->capacity);
	if (!vct->data)
		return (0);
	ft_memcpy(vct->data, old_data, vct->capacity * vct->unit_size);
	free(old_data);
	return (1);
}

int	vctr_push(t_vctr *vct, void *data)
{
	if (vct->entries == vct->capacity)
		if (!vctr_extend(vct))
			return (0);
	ft_memcpy(vct->data + vct->entries * vct->unit_size, data, vct->unit_size);
	vct->entries++;
	return (1);
}

int	str_to_vctr(t_vctr *vct, char *str)
{
	if (!str)
		return (0);
	while (*str)
	{
		if (!vctr_push(vct, str++))
			return (0);
	}
	return (1);
}

void	vctr_exit(t_vctr *vct)
{
	free(vct->data);
	free(vct);
}
