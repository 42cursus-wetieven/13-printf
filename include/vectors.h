/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vectors.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/03 16:18:02 by wetieven          #+#    #+#             */
/*   Updated: 2021/04/19 17:43:27 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef VECTORS_H
# define VECTORS_H

# include "libft.h"

typedef struct s_vctr {
	void			*data;
	size_t			entries;
	size_t			unit_size;
	size_t			init_count;
	size_t			size_factor;
	size_t			capacity;
}	t_vctr;

t_vctr	*vctr_init(size_t unit_size, size_t init_count);
int		vctr_extend(t_vctr *vct);
int		vctr_push(t_vctr *vct, void *data);
int		str_to_vctr(t_vctr *vct, char *str);
void	vctr_exit(t_vctr *vct);

#endif
